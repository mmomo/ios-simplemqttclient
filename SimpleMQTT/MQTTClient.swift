//
//  MQTTClient.swift
//  SimpleMQTT
//
//  Created by Enrique Venzor on 14/04/22.
//

import Foundation
import CocoaMQTT

class MQTTClient {
    let clientId: String = "CocoaMQTT-" + String(ProcessInfo().processIdentifier)
    let host: String = "192.168.1.8"
    let port: UInt16 = 1883
    
    let mqttUser = "wemos1"
    let mqttPass = "WEMOS0ne"
    
    var mqttClient: CocoaMQTT
    
    init () {
        mqttClient = CocoaMQTT(clientID: clientId,
                             host: host,
                             port: port)
    }
    
    func connect(host: String) {
        mqttClient = CocoaMQTT(clientID: clientId,
                             host: host,
                             port: port)
        
        mqttClient.username = mqttUser
        mqttClient.password = mqttPass
        
        // mqttClient.willMessage = CocoaMQTTMessage(topic: topic, string: "1")
        // mqttClient.keepAlive = 60
        
        mqttClient.delegate = self
        
        print("Connecting to \(host)")
        _ = mqttClient.connect()
    }
    
    func publish(message: String, topic: String) {
        mqttClient.publish(topic, withString: message, qos: .qos0)
    }
}

extension MQTTClient: CocoaMQTTDelegate {
    func mqtt(_ mqtt: CocoaMQTT, didConnectAck ack: CocoaMQTTConnAck) {
        print("didConnectAck")
    }
    
    func mqtt(_ mqtt: CocoaMQTT, didPublishMessage message: CocoaMQTTMessage, id: UInt16) {
        print("didPublishMessage")
        print(message)
    }
    
    func mqtt(_ mqtt: CocoaMQTT, didPublishAck id: UInt16) {
        print("didPublishAck")
    }
    
    func mqtt(_ mqtt: CocoaMQTT, didReceiveMessage message: CocoaMQTTMessage, id: UInt16) {
        print("didReceiveMessage")
        print(message)
    }
    
    func mqtt(_ mqtt: CocoaMQTT, didSubscribeTopics success: NSDictionary, failed: [String]) {
        print("didSubscribeTopics")
    }
    
    func mqtt(_ mqtt: CocoaMQTT, didUnsubscribeTopics topics: [String]) {
        print("didUnsusbscribeTopics")
    }
    
    func mqttDidPing(_ mqtt: CocoaMQTT) {
        print("didPing")

    }
    
    func mqttDidReceivePong(_ mqtt: CocoaMQTT) {
        print("didReceivePong")

    }
    
    func mqttDidDisconnect(_ mqtt: CocoaMQTT, withError err: Error?) {
        print("didDisconnect")

    }
    
    
}
