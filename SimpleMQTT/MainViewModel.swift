//
//  MainViewModel.swift
//  SimpleMQTT
//
//  Created by Enrique Venzor on 14/04/22.
//

import Foundation
import CocoaMQTT

extension ContentView {
    @MainActor class ViewModel: ObservableObject {
        @Published private var model = MQTTClient()
        
        func connectToBroker(host: String) {
            model.connect(host: host)
        }
        
        func publish(message: String, topic: String) {
            model.publish(message: message, topic: topic)
        }
        
    }
}
