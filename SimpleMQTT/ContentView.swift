//
//  ContentView.swift
//  SimpleMQTT
//
//  Created by Enrique Venzor on 14/04/22.
//

import SwiftUI
import CoreData

struct ContentView: View {
    @Environment(\.scenePhase) var scenePhase
    @Environment(\.managedObjectContext) private var viewContext

    @FetchRequest(
        sortDescriptors: [NSSortDescriptor(keyPath: \Broker.server, ascending: true)],
        animation: .default)
    private var brokers: FetchedResults<Broker>
    
    @StateObject private var viewModel = ViewModel()

    @State private var topic: String = ""
    @State private var message: String = ""
    @State private var server: String = ""
    
    var body: some View {
        VStack(alignment: .leading, spacing: 5.0) {
            Text("Write server direction")
                .padding()
            TextField("Message", text: $server)
                .padding(.horizontal, 16.0)
        }
        
        VStack {
            HStack {
                Text("Server:")
                    .bold()
                Text(brokers.last?.server ?? "")
            }
                        
            Button(action: {
                addItem(server: server)
                viewModel.connectToBroker(host: server)
            }) {
                Text("Connect")
            }
            .foregroundColor(.red)
            
            VStack(alignment: .leading, spacing: 5.0) {
                Text("Write a topic")
                    .padding()
                TextField("Topic", text: $topic)
                    .padding(.horizontal, 16.0)
            }
            
            
            VStack(alignment: .leading, spacing: 5.0) {
                Text("Write your message")
                    .padding()
                TextField("Message", text: $message)
                    .padding(.horizontal, 16.0)
            }
            
            Button(action: {
                viewModel.publish(message: message, topic: topic)
            }) {
                Text("PUBLISH")
            }
        }
    }
    
    private func addItem(server: String) {
        withAnimation {
            let broker = Broker(context: viewContext)
            broker.server = server

            do {
                try viewContext.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nsError = error as NSError
                fatalError("Unresolved error \(nsError), \(nsError.userInfo)")
            }
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
