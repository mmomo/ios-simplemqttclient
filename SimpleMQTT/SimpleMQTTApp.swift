//
//  SimpleMQTTApp.swift
//  SimpleMQTT
//
//  Created by Enrique Venzor on 14/04/22.
//

import SwiftUI

@main
struct SimpleMQTTApp: App {
    let persistenceController = PersistenceController.shared

    var body: some Scene {
        WindowGroup {
            ContentView()
                .environment(\.managedObjectContext, persistenceController.container.viewContext)
        }
    }
}
